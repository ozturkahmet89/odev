USE [DiyabetDB]
GO
/****** Object:  Table [dbo].[Doktor]    Script Date: 18.12.2016 22:53:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Doktor](
	[DoktorID] [int] IDENTITY(1,1) NOT NULL,
	[Bolum] [nvarchar](100) NOT NULL,
	[Hastane] [nvarchar](255) NOT NULL,
	[UyeID] [int] NOT NULL,
 CONSTRAINT [PK_Doktor] PRIMARY KEY CLUSTERED 
(
	[DoktorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DoktorNotu]    Script Date: 18.12.2016 22:53:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoktorNotu](
	[DoktorNotuID] [int] IDENTITY(1,1) NOT NULL,
	[MuayeneID] [int] NOT NULL,
	[DiyetProgrami] [nvarchar](max) NOT NULL,
	[BirSonrakiMuayeneTarihi] [datetime] NOT NULL,
	[HastaHatirlat] [bit] NOT NULL,
	[YakinHatirlat] [bit] NOT NULL,
 CONSTRAINT [PK_DoktorNotuID] PRIMARY KEY CLUSTERED 
(
	[DoktorNotuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hasta]    Script Date: 18.12.2016 22:53:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hasta](
	[HastaID] [int] IDENTITY(1,1) NOT NULL,
	[DogumTarihi] [date] NOT NULL CONSTRAINT [DF_Hasta_DogumTarihi]  DEFAULT (getdate()),
	[TCNo] [nvarchar](11) NOT NULL,
	[TelefonNo] [nvarchar](10) NOT NULL,
	[Adres] [nvarchar](max) NOT NULL,
	[YakinAdSoyad] [nvarchar](100) NULL,
	[YakinlikDerecesi] [nvarchar](100) NULL,
	[YakinMailAdresi] [nvarchar](100) NULL,
	[UyeID] [int] NOT NULL,
 CONSTRAINT [PK_Hasta] PRIMARY KEY CLUSTERED 
(
	[HastaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ilac]    Script Date: 18.12.2016 22:53:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ilac](
	[IlacID] [int] IDENTITY(1,1) NOT NULL,
	[IlacAdi] [nvarchar](100) NOT NULL,
	[MuayeneID] [int] NOT NULL,
	[GunlukDoz] [int] NOT NULL,
 CONSTRAINT [PK_Ilac] PRIMARY KEY CLUSTERED 
(
	[IlacID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Muayene]    Script Date: 18.12.2016 22:53:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Muayene](
	[MuayeneID] [int] IDENTITY(1,1) NOT NULL,
	[DosyaTarihi] [datetime] NOT NULL CONSTRAINT [DF_Muayene_DosyaTarihi]  DEFAULT (getdate()),
	[DoktorID] [int] NOT NULL,
	[HastaID] [int] NOT NULL,
	[ServisAdi] [nvarchar](100) NOT NULL,
	[MuayeneHizmeti] [nvarchar](50) NOT NULL,
	[Aciklama] [nvarchar](500) NOT NULL,
	[IstenenTahliller] [nvarchar](500) NULL,
 CONSTRAINT [PK_Muayene] PRIMARY KEY CLUSTERED 
(
	[MuayeneID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TahlilSonuclari]    Script Date: 18.12.2016 22:53:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TahlilSonuclari](
	[TahlilID] [int] IDENTITY(1,1) NOT NULL,
	[UyeID] [int] NOT NULL,
	[DosyaTarihi] [datetime] NOT NULL CONSTRAINT [DF_TahlilSonuclari_DosyaTarihi]  DEFAULT (getdate()),
	[Uyari] [nvarchar](50) NOT NULL,
	[TetkikIsmi] [nvarchar](100) NOT NULL,
	[TetkikSonucu] [nvarchar](100) NOT NULL,
	[TetkikReferansDegerleri] [nvarchar](max) NOT NULL,
	[MuayeneID] [int] NOT NULL,
 CONSTRAINT [PK_TahlilSonuclari] PRIMARY KEY CLUSTERED 
(
	[TahlilID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Uyeler]    Script Date: 18.12.2016 22:53:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Uyeler](
	[UyeID] [int] IDENTITY(1,1) NOT NULL,
	[AdiSoyadi] [nvarchar](50) NOT NULL,
	[KullaniciAdi] [nvarchar](50) NOT NULL,
	[Sifre] [nvarchar](50) NOT NULL,
	[Mail] [nvarchar](50) NOT NULL,
	[UyeTipi] [int] NOT NULL,
 CONSTRAINT [PK_Uyeler] PRIMARY KEY CLUSTERED 
(
	[UyeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Doktor] ON 

INSERT [dbo].[Doktor] ([DoktorID], [Bolum], [Hastane], [UyeID]) VALUES (1, N'Nöroloji', N'FSM', 2)
SET IDENTITY_INSERT [dbo].[Doktor] OFF
SET IDENTITY_INSERT [dbo].[DoktorNotu] ON 

INSERT [dbo].[DoktorNotu] ([DoktorNotuID], [MuayeneID], [DiyetProgrami], [BirSonrakiMuayeneTarihi], [HastaHatirlat], [YakinHatirlat]) VALUES (1, 7, N'hfngghngnghnghn', CAST(N'2016-12-14 00:00:00.000' AS DateTime), 1, 1)
INSERT [dbo].[DoktorNotu] ([DoktorNotuID], [MuayeneID], [DiyetProgrami], [BirSonrakiMuayeneTarihi], [HastaHatirlat], [YakinHatirlat]) VALUES (2, 7, N'fbfgdbdfhdfh', CAST(N'2016-12-08 00:00:00.000' AS DateTime), 1, 0)
INSERT [dbo].[DoktorNotu] ([DoktorNotuID], [MuayeneID], [DiyetProgrami], [BirSonrakiMuayeneTarihi], [HastaHatirlat], [YakinHatirlat]) VALUES (3, 7, N'fbfgdbdfhdfh', CAST(N'2016-12-08 00:00:00.000' AS DateTime), 1, 1)
INSERT [dbo].[DoktorNotu] ([DoktorNotuID], [MuayeneID], [DiyetProgrami], [BirSonrakiMuayeneTarihi], [HastaHatirlat], [YakinHatirlat]) VALUES (4, 3, N'nbnbnb', CAST(N'2016-12-15 00:00:00.000' AS DateTime), 1, 0)
SET IDENTITY_INSERT [dbo].[DoktorNotu] OFF
SET IDENTITY_INSERT [dbo].[Hasta] ON 

INSERT [dbo].[Hasta] ([HastaID], [DogumTarihi], [TCNo], [TelefonNo], [Adres], [YakinAdSoyad], [YakinlikDerecesi], [YakinMailAdresi], [UyeID]) VALUES (3, CAST(N'1989-09-08' AS Date), N'12345678901', N'0123456789', N'Serdivan', N'Kaya Bahçeli', N'Baba', N'kayabahceli@hotmail.com', 1)
INSERT [dbo].[Hasta] ([HastaID], [DogumTarihi], [TCNo], [TelefonNo], [Adres], [YakinAdSoyad], [YakinlikDerecesi], [YakinMailAdresi], [UyeID]) VALUES (7, CAST(N'1989-10-19' AS Date), N'01234567891', N'0123456789', N'nnjbg', N'can', N'abi', N'aydin@hotmail.com', 4)
SET IDENTITY_INSERT [dbo].[Hasta] OFF
SET IDENTITY_INSERT [dbo].[Ilac] ON 

INSERT [dbo].[Ilac] ([IlacID], [IlacAdi], [MuayeneID], [GunlukDoz]) VALUES (1, N'sdfsdfsd', 3, 2)
INSERT [dbo].[Ilac] ([IlacID], [IlacAdi], [MuayeneID], [GunlukDoz]) VALUES (2, N'gfhgjg', 3, 1)
INSERT [dbo].[Ilac] ([IlacID], [IlacAdi], [MuayeneID], [GunlukDoz]) VALUES (3, N'vermidon', 7, 5)
INSERT [dbo].[Ilac] ([IlacID], [IlacAdi], [MuayeneID], [GunlukDoz]) VALUES (4, N'şurup', 6, 3)
SET IDENTITY_INSERT [dbo].[Ilac] OFF
SET IDENTITY_INSERT [dbo].[Muayene] ON 

INSERT [dbo].[Muayene] ([MuayeneID], [DosyaTarihi], [DoktorID], [HastaID], [ServisAdi], [MuayeneHizmeti], [Aciklama], [IstenenTahliller]) VALUES (3, CAST(N'2016-12-12 00:00:00.000' AS DateTime), 1, 3, N'sdfsdfsdfs', N'dfgdfgdf', N'dfgdfgdfgdf', NULL)
INSERT [dbo].[Muayene] ([MuayeneID], [DosyaTarihi], [DoktorID], [HastaID], [ServisAdi], [MuayeneHizmeti], [Aciklama], [IstenenTahliller]) VALUES (4, CAST(N'2016-12-15 00:00:00.000' AS DateTime), 1, 3, N'Nöroloji', N'Kontrol', N'sfsdfsdvdfbd', NULL)
INSERT [dbo].[Muayene] ([MuayeneID], [DosyaTarihi], [DoktorID], [HastaID], [ServisAdi], [MuayeneHizmeti], [Aciklama], [IstenenTahliller]) VALUES (5, CAST(N'2016-12-08 00:00:00.000' AS DateTime), 1, 3, N'Nöroloji', N'Kontrol', N'dfgdfgdgdd', NULL)
INSERT [dbo].[Muayene] ([MuayeneID], [DosyaTarihi], [DoktorID], [HastaID], [ServisAdi], [MuayeneHizmeti], [Aciklama], [IstenenTahliller]) VALUES (6, CAST(N'2016-11-30 00:00:00.000' AS DateTime), 1, 3, N'Nöroloji', N'Kontrol', N'dsfsdfsdfs', NULL)
INSERT [dbo].[Muayene] ([MuayeneID], [DosyaTarihi], [DoktorID], [HastaID], [ServisAdi], [MuayeneHizmeti], [Aciklama], [IstenenTahliller]) VALUES (7, CAST(N'2016-12-16 00:00:00.000' AS DateTime), 1, 3, N'Nöroloji', N'Kontrol', N'sdfsdfsdfsdfsd', N'Üre,Kreatin,ALT,AST')
SET IDENTITY_INSERT [dbo].[Muayene] OFF
SET IDENTITY_INSERT [dbo].[TahlilSonuclari] ON 

INSERT [dbo].[TahlilSonuclari] ([TahlilID], [UyeID], [DosyaTarihi], [Uyari], [TetkikIsmi], [TetkikSonucu], [TetkikReferansDegerleri], [MuayeneID]) VALUES (4, 1, CAST(N'2016-12-15 00:00:00.000' AS DateTime), N'fdggdf', N'AST', N'sfsdfsdfsd', N'sdfsdfsdfsdf', 3)
INSERT [dbo].[TahlilSonuclari] ([TahlilID], [UyeID], [DosyaTarihi], [Uyari], [TetkikIsmi], [TetkikSonucu], [TetkikReferansDegerleri], [MuayeneID]) VALUES (5, 0, CAST(N'2016-12-18 12:53:26.447' AS DateTime), N'sonuç', N'Sonuç yok', N'sonuç Yok', N'Sonuç Yok', 5)
INSERT [dbo].[TahlilSonuclari] ([TahlilID], [UyeID], [DosyaTarihi], [Uyari], [TetkikIsmi], [TetkikSonucu], [TetkikReferansDegerleri], [MuayeneID]) VALUES (6, 0, CAST(N'2016-12-18 12:59:21.373' AS DateTime), N'Henüz Sonuç Çıkmadı', N'ALT', N'sonuç Yok', N'Sonuç Yok', 6)
INSERT [dbo].[TahlilSonuclari] ([TahlilID], [UyeID], [DosyaTarihi], [Uyari], [TetkikIsmi], [TetkikSonucu], [TetkikReferansDegerleri], [MuayeneID]) VALUES (7, 0, CAST(N'2016-12-23 00:00:00.000' AS DateTime), N'Düşük', N'Üre', N'cvfdgdfg', N'fdgdfgdfgdfg', 7)
SET IDENTITY_INSERT [dbo].[TahlilSonuclari] OFF
SET IDENTITY_INSERT [dbo].[Uyeler] ON 

INSERT [dbo].[Uyeler] ([UyeID], [AdiSoyadi], [KullaniciAdi], [Sifre], [Mail], [UyeTipi]) VALUES (1, N'Ahmet Can', N'ahmetcan', N'123456', N'ahmetcan@hotmail.com', 1)
INSERT [dbo].[Uyeler] ([UyeID], [AdiSoyadi], [KullaniciAdi], [Sifre], [Mail], [UyeTipi]) VALUES (2, N'admin', N'admin', N'123456', N'admin@mail.com', 0)
INSERT [dbo].[Uyeler] ([UyeID], [AdiSoyadi], [KullaniciAdi], [Sifre], [Mail], [UyeTipi]) VALUES (3, N'Ali', N'ali', N'123', N'ali@msn.com', 1)
INSERT [dbo].[Uyeler] ([UyeID], [AdiSoyadi], [KullaniciAdi], [Sifre], [Mail], [UyeTipi]) VALUES (4, N'Ahmet', N'ahmet', N'123', N'ali@msn.comahmet', 1)
SET IDENTITY_INSERT [dbo].[Uyeler] OFF
ALTER TABLE [dbo].[Doktor]  WITH CHECK ADD  CONSTRAINT [FK_Doktor_Uyeler] FOREIGN KEY([UyeID])
REFERENCES [dbo].[Uyeler] ([UyeID])
GO
ALTER TABLE [dbo].[Doktor] CHECK CONSTRAINT [FK_Doktor_Uyeler]
GO
ALTER TABLE [dbo].[DoktorNotu]  WITH CHECK ADD  CONSTRAINT [FK_DoktorNotuID_Muayene] FOREIGN KEY([MuayeneID])
REFERENCES [dbo].[Muayene] ([MuayeneID])
GO
ALTER TABLE [dbo].[DoktorNotu] CHECK CONSTRAINT [FK_DoktorNotuID_Muayene]
GO
ALTER TABLE [dbo].[Hasta]  WITH CHECK ADD  CONSTRAINT [FK_Hasta_Uyeler] FOREIGN KEY([UyeID])
REFERENCES [dbo].[Uyeler] ([UyeID])
GO
ALTER TABLE [dbo].[Hasta] CHECK CONSTRAINT [FK_Hasta_Uyeler]
GO
ALTER TABLE [dbo].[Ilac]  WITH CHECK ADD  CONSTRAINT [FK_Ilac_Muayene] FOREIGN KEY([MuayeneID])
REFERENCES [dbo].[Muayene] ([MuayeneID])
GO
ALTER TABLE [dbo].[Ilac] CHECK CONSTRAINT [FK_Ilac_Muayene]
GO
ALTER TABLE [dbo].[Muayene]  WITH CHECK ADD  CONSTRAINT [FK_Muayene_Doktor] FOREIGN KEY([DoktorID])
REFERENCES [dbo].[Doktor] ([DoktorID])
GO
ALTER TABLE [dbo].[Muayene] CHECK CONSTRAINT [FK_Muayene_Doktor]
GO
ALTER TABLE [dbo].[Muayene]  WITH CHECK ADD  CONSTRAINT [FK_Muayene_Hasta] FOREIGN KEY([HastaID])
REFERENCES [dbo].[Hasta] ([HastaID])
GO
ALTER TABLE [dbo].[Muayene] CHECK CONSTRAINT [FK_Muayene_Hasta]
GO
ALTER TABLE [dbo].[TahlilSonuclari]  WITH CHECK ADD  CONSTRAINT [FK_TahlilSonuclari_Muayene] FOREIGN KEY([MuayeneID])
REFERENCES [dbo].[Muayene] ([MuayeneID])
GO
ALTER TABLE [dbo].[TahlilSonuclari] CHECK CONSTRAINT [FK_TahlilSonuclari_Muayene]
GO
