namespace diyabet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DoktorNotu")]
    public partial class DoktorNotu
    {
        public int DoktorNotuID { get; set; }

        public int MuayeneID { get; set; }

        [Required]
        public string DiyetProgrami { get; set; }

        public DateTime BirSonrakiMuayeneTarihi { get; set; }

        public bool HastaHatirlat { get; set; }

        public bool YakinHatirlat { get; set; }

        public virtual Muayene Muayene { get; set; }
    }
}
