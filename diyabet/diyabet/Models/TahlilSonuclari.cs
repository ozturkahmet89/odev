namespace diyabet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TahlilSonuclari")]
    public partial class TahlilSonuclari
    {
        [Key]
        public int TahlilID { get; set; }

        public int UyeID { get; set; }

        public DateTime DosyaTarihi { get; set; }

        [Required]
        [StringLength(50)]
        public string Uyari { get; set; }

        [Required]
        [StringLength(100)]
        public string TetkikIsmi { get; set; }

        [Required]
        [StringLength(100)]
        public string TetkikSonucu { get; set; }

        [Required]
        public string TetkikReferansDegerleri { get; set; }

        public int MuayeneID { get; set; }

        public virtual Muayene Muayene { get; set; }
    }
}
