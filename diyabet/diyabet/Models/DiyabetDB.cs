namespace diyabet.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DiyabetDB : DbContext
    {
        public DiyabetDB()
            : base("name=DiyabetDB")
        {
        }

        public virtual DbSet<Doktor> Doktor { get; set; }
        public virtual DbSet<DoktorNotu> DoktorNotu { get; set; }
        public virtual DbSet<Hasta> Hasta { get; set; }
        public virtual DbSet<Ilac> Ilac { get; set; }
        public virtual DbSet<Muayene> Muayene { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<TahlilSonuclari> TahlilSonuclari { get; set; }
        public virtual DbSet<Uyeler> Uyeler { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Doktor>()
                .HasMany(e => e.Muayene)
                .WithRequired(e => e.Doktor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hasta>()
                .HasMany(e => e.Muayene)
                .WithRequired(e => e.Hasta)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Muayene>()
                .HasMany(e => e.DoktorNotu)
                .WithRequired(e => e.Muayene)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Muayene>()
                .HasMany(e => e.Ilac)
                .WithRequired(e => e.Muayene)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Muayene>()
                .HasMany(e => e.TahlilSonuclari)
                .WithRequired(e => e.Muayene)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Uyeler>()
                .HasMany(e => e.Doktor)
                .WithRequired(e => e.Uyeler)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Uyeler>()
                .HasMany(e => e.Hasta)
                .WithRequired(e => e.Uyeler)
                .WillCascadeOnDelete(false);
        }
    }
}
