namespace diyabet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ilac")]
    public partial class Ilac
    {
        public int IlacID { get; set; }

        [Required]
        [StringLength(100)]
        public string IlacAdi { get; set; }

        public int MuayeneID { get; set; }

        public int GunlukDoz { get; set; }

        public virtual Muayene Muayene { get; set; }
    }
}
