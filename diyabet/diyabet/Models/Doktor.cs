namespace diyabet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Doktor")]
    public partial class Doktor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Doktor()
        {
            Muayene = new HashSet<Muayene>();
        }

        public int DoktorID { get; set; }

        [Required]
        [StringLength(100)]
        public string Bolum { get; set; }

        [Required]
        [StringLength(255)]
        public string Hastane { get; set; }

        public int UyeID { get; set; }

        public virtual Uyeler Uyeler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Muayene> Muayene { get; set; }
    }
}
