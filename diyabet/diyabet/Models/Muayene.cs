namespace diyabet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Muayene")]
    public partial class Muayene
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Muayene()
        {
            DoktorNotu = new HashSet<DoktorNotu>();
            Ilac = new HashSet<Ilac>();
            TahlilSonuclari = new HashSet<TahlilSonuclari>();
        }

        public int MuayeneID { get; set; }

        public DateTime DosyaTarihi { get; set; }

        public int DoktorID { get; set; }

        public int HastaID { get; set; }

        [Required]
        [StringLength(100)]
        public string ServisAdi { get; set; }

        [Required]
        [StringLength(50)]
        public string MuayeneHizmeti { get; set; }

        [Required]
        [StringLength(500)]
        public string Aciklama { get; set; }

        [StringLength(500)]
        public string IstenenTahliller { get; set; }

        public virtual Doktor Doktor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DoktorNotu> DoktorNotu { get; set; }

        public virtual Hasta Hasta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ilac> Ilac { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TahlilSonuclari> TahlilSonuclari { get; set; }
    }
}
