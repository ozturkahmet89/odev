namespace diyabet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Hasta")]
    public partial class Hasta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Hasta()
        {
            Muayene = new HashSet<Muayene>();
        }

        public int HastaID { get; set; }

        [Column(TypeName = "date")]
        public DateTime DogumTarihi { get; set; }

        [Required]
        [StringLength(11)]
        public string TCNo { get; set; }

        [Required]
        [StringLength(10)]
        public string TelefonNo { get; set; }

        [Required]
        public string Adres { get; set; }

        [StringLength(100)]
        public string YakinAdSoyad { get; set; }

        [StringLength(100)]
        public string YakinlikDerecesi { get; set; }

        [StringLength(100)]
        public string YakinMailAdresi { get; set; }

        public int UyeID { get; set; }

        public virtual Uyeler Uyeler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Muayene> Muayene { get; set; }
    }
}
