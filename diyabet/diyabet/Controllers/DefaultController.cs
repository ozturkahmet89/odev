﻿using diyabet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace diyabet.Controllers
{   
    public class DefaultController : Controller
    {
        DiyabetDB context = new DiyabetDB();
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GirisYap(string kullaniciAdi, string parola)
        {
            Uyeler uye = context.Uyeler.FirstOrDefault(x => x.KullaniciAdi == kullaniciAdi);
            if (uye.Sifre == parola)
            {
                HttpCookie kulCookie = new HttpCookie("uye");
                kulCookie["ID"] = uye.UyeID.ToString();
                kulCookie["KullaniciAdi"] = uye.KullaniciAdi;
                kulCookie.Expires = DateTime.Now.AddMinutes(Session.Timeout);
                Response.Cookies.Add(kulCookie);
                kulCookie["uyeTipi"] = uye.UyeTipi.ToString();
                if (uye.UyeTipi == 0)
                    return RedirectToAction("Index", "Admin");
                else if (uye.UyeTipi == 1)
                    return RedirectToAction("Index", "Home");
                else if (uye.UyeTipi == 2)
                    return RedirectToAction("Index", "Doktor");
            }
            else
                ViewBag.Mesaj = "Kullanıcı Adı veya parola yanlış";
            return View();
        }

        public ActionResult KayitOl()
        {
            return View();
        }

       [HttpPost]
        public ActionResult KayitOl(Hasta yeniHasta, string Sifre, string SifreTekrar, string KullaniciAdi, string AdiSoyadi, string Mail)
        {
            Uyeler yeniUye = new Uyeler();
            if (Sifre == SifreTekrar)
            {
                yeniUye.UyeTipi         = 1;
                yeniUye.AdiSoyadi       = AdiSoyadi;
                yeniUye.Sifre           = Sifre;
                yeniUye.KullaniciAdi    = KullaniciAdi;
                yeniUye.Mail            = Mail;
                context.Uyeler.Add(yeniUye);
                context.SaveChanges();
                yeniHasta.UyeID = yeniUye.UyeID;
                yeniHasta.Uyeler = yeniUye;
                context.Hasta.Add(yeniHasta);
                
                context.Uyeler.Add(yeniUye);
                context.SaveChanges();
                return RedirectToAction("Index", "Home", new { });
            }
            else
            {
                ViewBag.Message = "Şifreler uyuşmuyor.";
                return View();
            }
        }
    }
}