﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace diyabet.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        diyabet.Models.DiyabetDB context = new Models.DiyabetDB();
        public ActionResult Index()
        {
            HttpCookie uye = Request.Cookies["uye"];
            return View(context.Hasta.FirstOrDefault(x => x.UyeID == Convert.ToInt32(uye["ID"])));
        }

        public ActionResult Muayeneler()
        {
            HttpCookie uye = Request.Cookies["uye"];
            return View(context.Muayene.Where(x => x.Hasta.UyeID == Convert.ToInt32(uye["ID"])));
        }
    }
}