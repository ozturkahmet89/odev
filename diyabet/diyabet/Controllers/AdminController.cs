﻿using diyabet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace diyabet.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        DiyabetDB context = new DiyabetDB();
        public ActionResult Index()
        {
            return View(context.Hasta);
        }

        public ActionResult HastaDosyasi(int id)
        {
            return View(context.Hasta.FirstOrDefault(x => x.HastaID == id));
        }

        public ActionResult Tahliller(int id)
        {
            return View(context.TahlilSonuclari.Where(x => x.MuayeneID == id));
        }

        public ActionResult MuayeneKaydiEkle(int id)
        {
            ViewBag.ID = id;
            return View();
        }

        [HttpPost]
        public ActionResult MuayeneKaydiEkle(Muayene m, int hastaID, string[] tahlil)
        {
            HttpCookie k        = Request.Cookies["uye"];
            m.DoktorID          = context.Doktor.ToList().FirstOrDefault(x => x.Uyeler.UyeID == Convert.ToInt32(k["ID"])).DoktorID;
            m.ServisAdi         = context.Doktor.ToList().FirstOrDefault(x => x.DoktorID == m.DoktorID).Bolum;
            m.Hasta             = context.Hasta.FirstOrDefault(x => x.HastaID == hastaID);
            m.Doktor            = context.Doktor.FirstOrDefault(x => x.DoktorID == m.DoktorID);
            m.IstenenTahliller  = string.Join(",", tahlil);
            context.Muayene.Add(m);
            context.SaveChanges();
            return RedirectToAction("HastaDosyasi", "Admin", new { id = hastaID });
        }

        public ActionResult TahlilSonucuEkle(int id)
        {
            return View(context.Muayene.FirstOrDefault(x => x.MuayeneID == id));
        }

        [HttpPost]
        public ActionResult TahlilSonucuEkle(TahlilSonuclari yeniTahlil, int id)
        {
            yeniTahlil.MuayeneID = id;
            yeniTahlil.Muayene = context.Muayene.FirstOrDefault(x => x.MuayeneID == id);
            context.TahlilSonuclari.Add(yeniTahlil);
            context.SaveChanges();
            return RedirectToAction("HastaDosyasi", "Admin", new { id = context.Muayene.FirstOrDefault(x => x.MuayeneID == id).HastaID });
        }

        public ActionResult Ilaclar(int id)
        {
            ViewBag.ID = id;
            return View(context.Ilac.Where(x => x.MuayeneID == id));
        }

        public ActionResult IlacEkle(int id)
        {
            return View(id);
        }

        [HttpPost]
        public ActionResult IlacEkle(Ilac yeniIlac, int id)
        {
            yeniIlac.MuayeneID = id;
            yeniIlac.Muayene = context.Muayene.FirstOrDefault(x => x.MuayeneID == id);
            context.Ilac.Add(yeniIlac);
            context.SaveChanges();
            return RedirectToAction("Ilaclar", "Admin", new { id = id});
        }

        public ActionResult NotuIncele(int id)
        {
            return View(context.DoktorNotu.FirstOrDefault(x => x.MuayeneID == id));
        }

        public ActionResult NotEkle(int id)
        {
            return View(id);
        }

        [HttpPost]
        public ActionResult NotEkle(DoktorNotu yeniNot, int id)
        {
            yeniNot.MuayeneID = id;
            yeniNot.Muayene = context.Muayene.FirstOrDefault(x => x.MuayeneID == id);
            context.DoktorNotu.Add(yeniNot);
            context.SaveChanges();
            return RedirectToAction("HastaDosyasi", "Admin", new { id = context.Muayene.FirstOrDefault(x => x.MuayeneID == id).HastaID });
        }
    }
}